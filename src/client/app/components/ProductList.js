import React from 'react'

import {Row} from 'react-bootstrap';

import style from '../css/global.css'

const Product = ({name, price, onClick}) => (
    <div onClick={onClick} className={style.itemDiv}>
        <span className={style.itemImg}>
            <img src="public/image.png"/>
        </span>
        <span className={style.priceTag}>{price}</span>
        <span className={style.itemName}>{name}</span>
    </div>
)


const ProductList = ({products, onProductClick}) => (
    <div>
        {products.map(prod => <Product
                                  key={prod.id}
                                  name={prod.name}
                                  price={prod.price}
                                  onClick={() => onProductClick(prod.id)}
                               />)
        }
    </div>
)

export default ProductList
