import React from 'react'
import ReactDOM from 'react-dom';

import _ from '../langs'
import getGlobalParam from '../consts'

import style from '../css/ticket.css'

class TicketRow extends React.Component {
    render() {
        return (
            <li
                onClick ={this.props.onRowClicked}
                className={style.orderLine + ' ' + (this.props.selected ? style.orderLineSelected : '')}
                >
                <span className={style.productName}>{this.props.name}</span>
                <span className={style.price}>{getGlobalParam("CURRENCY")} {this.props.price * this.props.qty}</span>
                <p> {_("QTY")} : <em>{this.props.qty}</em>|{ _("unitPrice")} : {this.props.price} {getGlobalParam("CURRENCY")} </p>
            </li>
        )
    }

    componentDidUpdate() {
        if (this.props.selected)
            this.props.scrollToAdjust(this)
    }

    componentDidMount() {
        if (this.props.selected)
            this.props.scrollToAdjust(this)
    }
}


class Ticket extends React.Component {

    render()
    {
        return (
            <div className={style.wrapTicket}>
                <div className={style.totalPrice}>{this.props.total}</div>
                <div className={style.tabTicket} ref='cont'>
                    <ul className={style.orderLines}>
                        {this.props.rows.map(row => <TicketRow
                                                        key={row.id}
                                                        name={row.name}
                                                        price={row.price}
                                                        qty={row.qty}
                                                        selected={this.props.selectedTicketRow == row.id}
                                                        onRowClicked={() => this.props.onRowClicked(row.id)}
                                                        scrollToAdjust={(elt) => {this.scrolltocontain(this.refs.cont, elt)}}/>)
                        }
                    </ul>
                </div>
            </div>
        );
    }

    scrolltocontain(cont, elt)
    {
        elt = ReactDOM.findDOMNode(elt);
        cont = ReactDOM.findDOMNode(cont);
        if (elt.offsetTop < cont.scrollTop || elt.offsetTop > cont.offsetHeight + cont.scrollTop) {
            cont.scrollTop = elt.offsetTop;
        }
    }
}

export default Ticket
