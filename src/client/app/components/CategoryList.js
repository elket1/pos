import React from 'react'
import {Row} from 'react-bootstrap';

import style from '../css/global.css'

const Category = ({name, onClick}) => (
    <div onClick={onClick} className={style.itemDiv}>
        <span className={style.itemImg}>
            <img src="public/image.png"/>
        </span>
        <span className={style.itemName}>
            {name}
        </span>
    </div>

)

const CategoryList = ({categories, onCategoryClick}) => (
    <div>
        {categories.map(cat => <Category key={cat.id} name={cat.name} onClick={() => onCategoryClick(cat.id)}/>)}
    </div>
)

export default CategoryList
