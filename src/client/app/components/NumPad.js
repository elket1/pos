import React from 'react'

import style from '../css/editpan.css'


// customButtons is an array  of 2 object like this [{label:"Qty", onClick:Callback}, {label:"Del", onClick:Callback, val:"value1"}]

const NumPad = ({onNumberClick, customButtons}) => (
    <div className={style.numpad}>
        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
            .map(elt => <button key={elt}
                            onClick={(e) => {e.preventDefault(); onNumberClick(elt); }}
                            className={style.inputButtonNumber}>{elt}</button>)
        }
        {customButtons.map(obj =>  <button
                                        key={obj.label}
                                        onClick={(e) => {e.preventDefault(); obj.onClick(obj.val);}}
                                        className={style.inputButtonNumber + ' ' + style.inputButtonTxt}>{obj.label}</button>)
        }
    </div>
)

export default NumPad;
