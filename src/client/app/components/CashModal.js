import React from 'react';

import Modal from '../modals';
import NumPad from './NumPad'
import ReceiptTicket from './ReceiptTicket'
import {PrintTicket} from '../helpers'
import _ from '../langs'
import getGlobalParam from '../consts'
import style from '../css/editpan.css'
import styleTicket from '../css/ticket.css'

import 'print.js'

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        width: '300px',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

/*const CashModal = ({
    isCashModalOpen,
    total,
    inputAmount,
    ticketToprint,
    onClose,
    onNumberClick,
    onValidateClick,
    validated,
}) =>*/

class CashModal extends React.Component {

    checkAmountTotal(total, inputAmount){
        return (total > 0 && (total < inputAmount || total == inputAmount)) ? true : false;
    }

    render() {
        if (this.props.validated === false){
            return (
                <Modal isOpen={this.props.isCashModalOpen} onClose={this.props.onClose}>
                        <div className = {styleTicket.modalTotalPrice}>
                            <span className = {styleTicket.supTag}>{_("total")}</span>
                            {this.props.total}
                        </div>
                        <div className = {styleTicket.modalTotalPrice} style={{marginTop: '5px',color:'green'}}>
                            <span className = {styleTicket.supTag}>{_("paid")}</span>
                            {this.props.inputAmount}
                        </div>
                        <br />
                        <p className = {styleTicket.changeTag}>
                            {_("change")} : {(Number(this.props.inputAmount)-this.props.total>0) ?
                            Number(this.props.inputAmount)-this.props.total : 0}
                        </p>
                        <NumPad
                            className = {style.numpad}
                            onNumberClick = {this.props.onNumberClick}
                            customButtons = {
                                [
                                    { label: ".", onClick: this.props.onNumberClick, val: "."},
                                    { label: _("Del"), onClick: this.props.onNumberClick, val: "-1"}
                                ]
                            }
                        />
                        <div  className = {style.buttonpad}>
                            <button className = {style.inputButtonPad}
                                onClick={(e)=>{e.preventDefault(); this.props.onNumberClick("+" + this.props.total);}}
                            >{getGlobalParam('CURRENCY')  + ' ' + this.props.total}</button>
                            <button
                                className = {(this.checkAmountTotal(Number(this.props.total), Number(this.props.inputAmount)))?
                                            style.inputButtonPad + " " + style.inputButtonPadGreen : style.inputButtonPad + " " + style.inputButtonPadDeactivated}
                                onClick={(e)=>{e.preventDefault(); this.props.onValidateClick();}}
                                disabled={! this.checkAmountTotal(Number(this.props.total), Number(this.props.inputAmount))}
                            >{_("Valider")}</button>
                            <button className = {style.inputButtonPad}
                                onClick={(e)=>{e.preventDefault(); this.props.onClose();}}
                            >{_("Fermer")}</button>
                        </div>
                </Modal>
            )
        }

        else if (this.props.validated === true) {
            return  (
                <Modal isOpen={this.props.isCashModalOpen} onClose={this.props.onClose} noClickBackdrop>
                    <div className = {styleTicket.modalTotalPrice} style={{background:'none'}}>
                        <span className = {styleTicket.supTag}>{_("change")}</span>
                        {(Number(this.props.inputAmount)-this.props.total>0) ?
                        Number(this.props.inputAmount)-this.props.total : 0}
                    </div>
                    <button className={style.inputButtonPad}
                        onClick={(e)=>{e.preventDefault(); this.props.onNewOrderClick();}}
                    >{_("newOrder")}</button>
                    <button className={style.inputButtonPad}
                        onClick={() => PrintTicket("ReceiptTicket")}
                    >{_("print")}</button>
                    <div id="ReceiptTicket">
                        <ReceiptTicket
                            rows ={this.props.ticketToprint}
                            total = {this.props.total}
                            inputAmount = {this.props.inputAmount}
                            companyName = {getGlobalParam("COMPANY_NAME")}
                        />
                    </div>
                </Modal>
            )
        }
    }
}
//	  <button onClick={this.closeModal}>close</button>

export default CashModal
