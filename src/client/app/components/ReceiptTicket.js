import React from 'react'

import {getDate} from '../helpers'
import _ from '../langs'
import getGlobalParam from '../consts'

import style from '../css/receipt.css'

const ReceiptTicket = ({rows, total, companyName, inputAmount}) => (
    < div className = {style.posSaleTicket} >
        <div className={style.posCenterAlign}>{getDate()}</div>
        < br /> {getGlobalParam("COMPANY_NAME")} < br /> User : Administrator < br /> <br />
        <table className={style.receiptOrderlines}>
            <colgroup>
                <col width="50%"/>
                <col width="25%"/>
                <col width="25%"/>
            </colgroup>
            <tbody>
            {rows.map((row)=>
                <tr key={row.id}>
                    <td>
                        {row.name}
                    </td>
                    <td className={style.posRightAlign}>
                        {row.qty}
                    </td>
                    <td className={style.posRightAlign}>
                        {getGlobalParam("CURRENCY")} {row.price}
                    </td>
                </tr>
            )}
            </tbody>
        </table>
        <br/>
        <table className={style.receiptTotal}>
            <tbody>
                <tr>
                    <td>{_("total")}:</td>
                    <td className={style.posRightAlign}>
                        {getGlobalParam("CURRENCY")} {total}
                    </td>
                </tr>

                <tr>
                    <td>{_("tax")} {getGlobalParam("TAX")}%</td>
                    <td className={style.posRightAlign}>
                        {getGlobalParam("CURRENCY")} {total * getGlobalParam("TAX") / 100}
                    </td>
                </tr>
            </tbody>
        </table>
        <br/>
        <table className={style.receiptPaymentlines}>

            <tbody>
                <tr>
                    <td>
                        {_("paid")}
                    </td>
                    <td className={style.posRightAlign}>
                        {getGlobalParam("CURRENCY")} {inputAmount}
                    </td>
                </tr>

            </tbody>
        </table>
        <br/>
            <table className={style.receiptChange}>
                <tbody>
                    <tr>
                        <td>{_("change")}:</td>
                        <td className={style.posRightAlign}>
                            {getGlobalParam("CURRENCY")} {inputAmount - total}
                        </td>
                    </tr>
                </tbody>
            </table>
    </div>
)


export default ReceiptTicket
