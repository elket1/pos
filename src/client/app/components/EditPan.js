import React from 'react'

import style from '../css/editpan.css'
import NumPad from './NumPad'
import _ from '../langs'


const EditPan = ({onNumberClick, onCashClick, onDeleteClick}) => (
    <div>
        <div className={style.buttonpad}>
            <button
                className={style.inputButtonPad}
                onClick={onCashClick}>{_("CASH")}</button>
        </div>
        <NumPad
            onNumberClick={onNumberClick}
            customButtons={ [{label:_("Qty"), onClick:()=>{}}, {label:_("Del"), onClick:onDeleteClick}] }
        />
    </div>
)

export default EditPan
