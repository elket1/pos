//import { combineReducers } from 'redux'
import {createStore} from 'redux'

import {
    SELECT_CAT,
    BUY_PROD,
    DETELE_ROW,
    EDIT_QTY,
    SELECT_ROW_TICKET,
    OPEN_CASH_MODAL,
    CLOSE_CASH_MODAL,
    CHANGE_MONEY_AMOUNT,
    VALIDATE_TICKET,
    NEW_ORDER
} from './actions'
import {orm} from './models';

//Init Db
const emptyDBState = orm.getEmptyState();
const session = orm.session(emptyDBState);
const {ProductMod, TicketLineMod, TicketMod, CategoryMod}  = session;

const catMeal = CategoryMod.create({name: 'Drinks'})
const catDrink = CategoryMod.create({name: 'Fruits'})
ProductMod.create({name: 'Apple', category: catMeal.id, price: 10})
ProductMod.create({name: 'Orange', category: catMeal.id, price: 20})
ProductMod.create({name: 'Bannana', category: catMeal.id, price: 20})
ProductMod.create({name: 'Strawberry', category: catMeal.id, price: 20})
ProductMod.create({name: 'Peach', category: catMeal.id, price: 10})
ProductMod.create({name: 'Raisin', category: catMeal.id, price: 10})
ProductMod.create({name: 'Mûres', category: catMeal.id, price: 10})
ProductMod.create({name: 'Blackberries', category: catMeal.id, price: 10})
ProductMod.create({name: 'Coca', category: catDrink.id, price: 10})
ProductMod.create({name: 'Pepsi', category: catDrink.id, price: 10})

const currentTicket = TicketMod.create({validated:false})

const initialDbState = session.state;

let initialState = {
    selectedCat: 0,
    dbState: initialDbState,
    selectedTicketRow: null,
    firstChar: true,
    isCashModalOpen: false,
    inputAmount: "0",
    currentTicketId: currentTicket.id,
}

function posApp(state = initialState, action) {

    const sess = orm.session(state.dbState);
    switch (action.type) {

        case SELECT_CAT:
            return Object.assign({}, state, {selectedCat: action.id})

        case BUY_PROD:
            {
                const {TicketLineMod, ProductMod} = sess;

                let qs = TicketLineMod.filter({product:action.id, ticket:state.currentTicketId})
                let rowId;

                if (qs.count() == 0) {
                    rowId = TicketLineMod.create({product: action.id, ticket: state.currentTicketId, qty: 1}).id;
                } else {
                    const ticketRow = qs.first()
                    const oldQty = ticketRow.qty
                    ticketRow.update({
                        qty: oldQty + 1
                    })
                    rowId = ticketRow.id;
                }

                return Object.assign({}, state, {
                    dbState: sess.state,
                    selectedTicketRow: rowId,
                    firstChar: true
                })
            }

        case DETELE_ROW:
            {
                const {TicketLineMod} = sess;

                if (state.selectedTicketRow !== null) {

                    if (TicketLineMod.withId(state.selectedTicketRow).qty != 1) {
                        TicketLineMod
                            .withId(state.selectedTicketRow)
                            .update({qty: 1});

                        return Object.assign({}, state, {
                            dbState: sess.state,
                            firstChar: true
                        })
                    } else {
                        TicketLineMod
                            .withId(state.selectedTicketRow)
                            .delete();

                        return Object.assign({}, state, {
                            dbState: sess.state,
                            selectedTicketRow: null,
                            firstChar: true
                        })
                    }

                }
                return state;
            }

        case EDIT_QTY:
            {
                if (state.selectedTicketRow !== null) {

                    const {TicketLineMod} = sess;
                    let qty;
                    if (state.firstChar) {
                        qty = action.val;
                    } else {
                        qty = "" + TicketLineMod
                            .withId(state.selectedTicketRow)
                            .qty + action.val; //Force it to concatenate as char
                    }

                    TicketLineMod
                        .withId(state.selectedTicketRow)
                        .update({qty: Number(qty)});

                    return Object.assign({}, state, {
                        dbState: sess.state,
                        firstChar: false
                    });
                } else {
                    return state;
                }
            }

        case SELECT_ROW_TICKET:
            {
                return Object.assign({}, state, {
                    selectedTicketRow: action.id,
                    firstChar: true
                });
            }

        case OPEN_CASH_MODAL:
            {
                return Object.assign({}, state, {isCashModalOpen: true});
            }

        case CLOSE_CASH_MODAL:
            {
                return Object.assign({}, state, {isCashModalOpen: false});
            }

        case CHANGE_MONEY_AMOUNT:
            {
                let amount, savedAmount = state.inputAmount;
                action.val = action.val.toString()

                if (action.val.includes("+")){
                    amount  = action.val.substr(1, action.val.length);
                }
                else if (state.inputAmount === "0" && action.val !== "-1") {
                    amount  = action.val;
                } else if (action.val === "-1"){
                    amount = state.inputAmount.substr(0, state.inputAmount.length - 1);
                }
                else {
                    amount = state.inputAmount + action.val; //Force it to concatenate as char
                }
                amount = (amount !== "") ? amount : "0";
                amount = (!isNaN(amount)) ? amount : savedAmount;
                return Object.assign({}, state, {inputAmount: amount.toString()});
            }

        case VALIDATE_TICKET:
            {
                const {TicketMod} = sess;
                TicketMod.withId(state.currentTicketId).update({validated:true})
                return Object.assign({}, state, {dbState: sess.state});
            }

        case NEW_ORDER:
            {
                const {TicketMod} = sess;
                const newTicketId = TicketMod.create({validated:false}).id
                return Object.assign({}, state, {
                    currentTicketId: newTicketId,
                    dbState: sess.state,
                    selectedTicketRow: null,
                    firstChar: true,
                    isCashModalOpen: false,
                    inputAmount: "0",
                    selectedCat: 0
                });
            }

        default:
            return state
    }
}

/*const todoApp = combineReducers({
  visibilityFilter,
  todos
})*/
let store = createStore(posApp)
export default store
