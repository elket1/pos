import React, {Component} from 'react';

import {selectCat} from './actions'
import CategoryList from './components/CategoryList'
import VisibleProductList from './containers/VisibleProducts'
import VisibleCategoryList from './containers/VisibleCats'
import VisibleTicket from './containers/VisibleTicket'
import VisibleEditPan from './containers/VisibleEditPan'
import VisibleCashModal from './containers/VisibleCashModal'
import {Grid, Row, Col} from 'react-bootstrap';

import style from './css/app.css'

class App extends Component {

    render() {
        return (
            <div>
                <div className={style.topBar}></div>
                <Grid fluid className={style.contentPan}>
                    <Row >
                        <Col sm={8} xs={8} md={4} className={style.leftpanDiv}>
                            <Row className={style.ticketDiv}>
                                <Col xs={12} md={12} className={style.nopadding}>

                                    <VisibleTicket/>

                                </Col>
                            </Row>
                            <Row className={style.editpanDiv}>
                                <Col xs={12} md={12} className={style.nopadding}>

                                    <VisibleEditPan/>

                                </Col>
                            </Row>
                        </Col>
                        <Col sm={4} xs={4} md={8} className={style.rightpanDiv}>
                            <Row className={style.catsDiv}>
                                <Col xs={12} md={12}>

                                    <VisibleCategoryList/>

                                </Col>
                            </Row>
                            <Row className={style.productsDiv}>
                                <Col xs={12} md={12}>

                                    <VisibleProductList/>

                                </Col>
                            </Row>
                        </Col>
                    </Row>

                    <VisibleCashModal/>
                    
                </Grid>
            </div>
        );
    }
}

export default App;
