import { connect } from 'react-redux'
import {orm} from '../models';
import Ticket from '../components/Ticket'
import {onDeleteClick , selectRowTicket} from '../actions'
import {calculTotal} from '../helpers'

const mapStateToProps = (state) => {

	const sess = orm.session(state.dbState);
    const {TicketLineMod} = sess;
	let arr = [];

    arr = TicketLineMod.filter({ticket:state.currentTicketId}).toModelArray().map
	(
		(row) =>
		{
			return Object.assign(row.product._fields, {qty:row.qty, id:row.id});
		}
	)

	var total = calculTotal(TicketLineMod.filter({ticket:state.currentTicketId}).toModelArray())
	return {rows: arr,
            total: total,
            selectedTicketRow: state.selectedTicketRow}
}


const mapDispatchToProps = (dispatch) => {
  return {
	onRowClicked: (id) => {
		dispatch(selectRowTicket(id))
	}
  }
}


const VisibleTicket = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Ticket)

export default VisibleTicket
