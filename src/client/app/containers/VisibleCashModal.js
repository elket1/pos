import {connect} from 'react-redux'

import {orm} from '../models';
import CashModal from '../components/CashModal'
import {closeCashModal, changeMoneyInput, validateTicket, newOrder} from '../actions'
import {calculTotal} from '../helpers'


const mapStateToProps = (state) => {

    const sess = orm.session(state.dbState);
    const {TicketLineMod, TicketMod} = sess
    var total = calculTotal(TicketLineMod.filter({ticket:state.currentTicketId}).toModelArray())

    var arr = []
    arr = TicketLineMod.filter({ticket:state.currentTicketId}).toModelArray()
            .map((row) => {return Object.assign(row.product._fields, {
                                qty: row.qty,
                                id: row.id});})

    return {isCashModalOpen: state.isCashModalOpen,
            total: total,
            inputAmount: state.inputAmount,
            ticketToprint: arr,
            validated : TicketMod.withId(state.currentTicketId).validated}
}


const mapDispatchToProps = (dispatch) => {
    return {
        onClose: () => {
            dispatch(closeCashModal())
        },
        onNumberClick: (val) => {
            dispatch(changeMoneyInput(val))
        },
        onValidateClick: () => {
            dispatch(validateTicket())
        },
        onNewOrderClick : () => {
            dispatch(newOrder())
        },
    }
}

const VisibleCashModal = connect(mapStateToProps, mapDispatchToProps,)(CashModal)

export default VisibleCashModal
