import {connect} from 'react-redux'

import {orm} from '../models';
import ProductList from '../components/ProductList'
import {buyProd} from '../actions'


const getVisibleProds = (CategoryMod, selectedCat) => {
    if (CategoryMod.hasId(selectedCat)) {
        return CategoryMod.withId(selectedCat).productmodSet.toRefArray()
    } else {
        return []
    }
}


const mapStateToProps = (state) => {
    const sess = orm.session(state.dbState);
    const {CategoryMod} = sess
    return {
        products: getVisibleProds(CategoryMod, state.selectedCat)
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        onProductClick: (id) => {
            dispatch(buyProd(id))
        }
    }
}


const VisibleProductList = connect(mapStateToProps, mapDispatchToProps,)(ProductList)

export default VisibleProductList
