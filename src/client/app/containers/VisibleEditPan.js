import {connect} from 'react-redux'

import {orm} from '../models';
import EditPan from '../components/EditPan'
import {editqty, openCashModal, onDeleteClick} from '../actions'


const mapDispatchToProps = (dispatch) => {
    return {
        onNumberClick: (val) => {
            dispatch(editqty(val))
        },
        onCashClick: () => {
            dispatch(openCashModal())
        },
        onDeleteClick: () => {
            dispatch(onDeleteClick())
        }
    }
}


const VisibleEditPan = connect(null, //mapStateToProps,
        mapDispatchToProps)(EditPan)

export default VisibleEditPan
