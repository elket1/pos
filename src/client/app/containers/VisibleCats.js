import {connect} from 'react-redux'

import {orm} from '../models';
import CategoryList from '../components/CategoryList'
import {selectCat} from '../actions'


const mapStateToProps = (state) => {
    const sess = orm.session(state.dbState);
    const {CategoryMod} = sess
    return {categories: CategoryMod.all().toRefArray()}
}


const mapDispatchToProps = (dispatch) => {
    return {
        onCategoryClick: (id) => {
            dispatch(selectCat(id))
        }
    }
}

const VisibleCategoryList = connect(mapStateToProps, mapDispatchToProps)(CategoryList)

export default VisibleCategoryList
