const globalParam = {
    'CURRENCY' : 'DZD',
    'COMPANY_NAME' : 'Demo company',
    'TAX' : 19,
}

const getGlobalParam = (param) => {
    return globalParam[param];
}
export default getGlobalParam;
