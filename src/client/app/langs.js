const SELECTED_LANG = 'fr';

const fr = {
    'change' : 'Monnaie',
    'total' : 'Total',
    'paid' : 'Payé',
    'print' : 'Imprimer',
    'newOrder' : 'Nouvelle commande',
    'unitPrice' : 'Prix Unitaire',
    'close' : 'Close',
    'Validate' : 'Valider',
    'tax' : 'taxe',
}

const langs = {
    'fr' : fr,
    //'en' : en,
}

const  _ = (str) => {
    return  (str in langs[SELECTED_LANG]) ? langs[SELECTED_LANG][str] : str;
}

export default _
