import {ORM} from 'redux-orm';
import {fk, many, attr, Model} from 'redux-orm';

class CategoryMod extends Model {

    static get modelName() {
        return 'CategoryMod';
    }

    static get fields() {
        return {id: attr(), name: attr()};
    }
}


class ProductMod extends Model {

    static get modelName() {
        return 'ProductMod';
    }

    static get fields() {
        return {id: attr(), name: attr(), price: attr(), category: fk('CategoryMod')};
    }
}


class TicketLineMod extends Model {

    static get modelName() {
        return 'TicketLineMod';
    }

    static get fields() {
        return {id: attr(), qty: attr(), product: fk('ProductMod'), ticket: fk('TicketMod')};
    }
}


class TicketMod extends Model {

    static get modelName() {
        return 'TicketMod';
    }

    static get fields() {
        return {id: attr(), validated: attr()};
    }
}


const orm = new ORM();
orm.register(CategoryMod, ProductMod, TicketMod, TicketLineMod);
export {orm}
