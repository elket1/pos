export function calculTotal(arr) {
    var total = 0;
    arr.forEach((row) => {
        total = total + (row.product.price * row.qty)
    });
    return total;
}

export function getDate()
{
    let d = new Date();
    return d.getUTCFullYear() + '/' + d.getMonth() + '/'
            + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes();
}

export function PrintTicket(elem) {

    var w = window.open('', 'PRINT', 'height=400,width=600');
    jQuery.get('app/css/receipt.css', function (data) {
        w.document.write('<html><head><style>');
        w.document.write(data);
        w.document.write('</style></head><body>');
        w.document.write($('#'+elem).html());
        w.document.write('</body></html>');
        w.print();
        //w.close();
    });
    return true;
}
