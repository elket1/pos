export const SELECT_CAT = 'SELECT_CAT'
export const BUY_PROD = 'BUY_PROD'
export const DETELE_ROW = 'DETELE_ROW'
export const EDIT_QTY = 'EDIT_QTY'
export const SELECT_ROW_TICKET = 'SELECT_ROW_TICKET'
export const OPEN_CASH_MODAL = 'OPEN_CASH_MODAL'
export const CLOSE_CASH_MODAL = 'CLOSE_CASH_MODAL'
export const CHANGE_MONEY_AMOUNT = 'CHANGE_MONEY_AMOUNT'
export const VALIDATE_TICKET = 'VALIDATE_TICKET'
export const NEW_ORDER = 'NEW_ORDER'


export function selectCat(id) {
    return {type: SELECT_CAT, id}
}

export function buyProd(id) {
    return {type: BUY_PROD, id: id}
}

export function onDeleteClick() {
    return {type: DETELE_ROW}
}

export function editqty(val) {
    return {type: EDIT_QTY, val: val}
}

export function selectRowTicket(id) {
    return {type: SELECT_ROW_TICKET, id: id}
}

export function openCashModal() {
    return {type: OPEN_CASH_MODAL}
}

export function closeCashModal() {
    return {type: CLOSE_CASH_MODAL}
}

export function changeMoneyInput(val) {
    return {type: CHANGE_MONEY_AMOUNT, val: val}
}

export function validateTicket() {
    return {type: VALIDATE_TICKET}
}

export function newOrder() {
    return {type: NEW_ORDER}
}
