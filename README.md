<a href="https://gitlab.com/elket1/pos/commits/master"><img alt="build status" src="https://gitlab.com/elket1/pos/badges/master/build.svg" /></a><br/><br/>
This is a hobby project, the design of the UI was inspired from the odoo erp pos.<br/>
This is a Reactjs/Redux based point of sale<br/>
You can see a <b> demo </b>and the evolution of the project on  http://elket1.gitlab.io/pos/
